DROP SCHEMA 

IF EXISTS DD5;
	CREATE SCHEMA DD5 COLLATE = utf8_danish_ci;

USE DD5;


create table skier  (
  userName		varchar(10) NOT NULL,
  firstName		varchar(20) NOT NULL, 
  lastName		varchar(20) NOT NULL,
  yearOfBirth	YEAR NOT NULL,	
  primary key(userName)
   
);


create table club (
  id      		varchar(15) NOT NULL,
  name    		varchar(20) NOT NULL,
  city			varchar(20) NOT NULL,
  county		varchar(40) NOT NULL,
  primary key(id)
);

create table season (
  seasonYear    YEAR NOT NULL,
  primary key(seasonYear)
);

create table seasonRegister (
  seasonYear    YEAR NOT NULL,
  clubId		varchar(15),
  userName		varchar(10) NOT NULL,
  primary key(seasonYear,userName),
  foreign key(seasonYear)
	references season(seasonYear),
  foreign key(clubId)
	references club(id),
  foreign key(userName)
	references skier(userName)
);


create table entry (
  eDate     	date NOT NULL,
  area			varchar(15) NOT NULL,
  distance		int(4) NOT NULL,
  userName		varchar(10) NOT NULL,
  primary key(eDate,userName),
  foreign key(userName)
	references skier(userName)
);

create table seasonDistance (
  seasonYear 	year NOT NULL,
  userName		varchar(10) NOT NULL,
  totalDist		int(9),
  primary key(seasonYear,userName),
  foreign key(seasonYear)
	references season(seasonYear),
  foreign key(userName)
	references skier(userName)
);
