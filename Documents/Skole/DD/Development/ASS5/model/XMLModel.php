<?php
include_once("Skier.php");
include_once("Entry.php");
include_once("Club.php");
include_once("Register.php");

class DOMmodel {
	
	//public $doc;
	
	
	public function __construct() {
		
		//$this->doc = new DOMDocument();
		//$this->doc->load('model/SkierLogs.XML');
		
		
		//$this->xpath = new DOMXPath($this->doc);
		
	}




	/*
	READS SKIERS AND THEIR ENTRIES.
	RETURNS ARRAY WITH SKIER OBJECTS WHERE EACH SKIER CONTAINS AN ARRAY OF ENTRY OBJECTS.
	*/

	public function readSkiers() {
		
		$doc = new DOMDocument();
		$doc->load('model/SkierLogs.XML');
		$xpath = new DOMXPath($doc);
		
		$elements = $xpath->query("/SkierLogs/Skiers/Skier");
		foreach ($elements as $element)  {
	
			$userName = $element->getAttribute("userName"); 
	
			$firstNames = $element->getElementsByTagName( "FirstName");
			$firstName = $firstNames->item(0)->nodeValue;
	
			$lastNames = $element->getElementsByTagName("LastName");
			$lastName = $lastNames->item(0)->nodeValue;
	
			$yearOfBirths = $element->getElementsByTagName("YearOfBirth");
			$yearOfBirth = $yearOfBirths->item(0)->nodeValue;
			
			$distance15 = $xpath->evaluate("sum(/SkierLogs/Season[@fallYear = '2015']/Skiers/Skier[@userName ='$userName']/Log/Entry/Distance)");
			$distance16 = $xpath->evaluate("sum(/SkierLogs/Season[@fallYear = '2016']/Skiers/Skier[@userName ='$userName']/Log/Entry/Distance)");
			$entryelements = $xpath->query("/SkierLogs/Season/Skiers/Skier[@userName = '$userName']/Log/Entry");
			
			$entries = null;
			foreach ($entryelements as $entryelement)  {
				
				$dates = $entryelement->getElementsByTagName( "Date");
				$date = $dates->item(0)->nodeValue;
	
				$areas = $entryelement->getElementsByTagName("Area");
				$area = $areas->item(0)->nodeValue;
	
				$distances = $entryelement->getElementsByTagName("Distance");
				$distance = $distances->item(0)->nodeValue;
		
		
				$entries[] = new Entry($date,$area,$distance);
			}
	
			
			$skiers[] = new Skier ($userName, $firstName,$lastName,$yearOfBirth,$entries,$distance15,$distance16);
			
		}
		
		return $skiers;
	}
	
	
	/*
	READS CLUBS AND SKIER USERNAMES ASSIGNED FOR THAT PARTICULAR CLUB FOR A GIVEN SEASON.
	RETURNS AN ARRAY OF CLUB OBJECTS WHICH CONTAINS TWO ARRAYS OF REGISTER OBJECTS.
	*/
	public function readClubs() {
		
		$doc = new DOMDocument();
		$doc->load('model/SkierLogs.XML');
		$xpath = new DOMXPath($doc);
		$elements = $xpath->query("/SkierLogs/Clubs/Club");
		foreach ($elements as $element)  {
			$skier2015 = null;
			$skier2016 = null;
			$id = $element->getAttribute("id"); 
	
			$names = $element->getElementsByTagName( "Name");
			$name = $names->item(0)->nodeValue;
	
			$cities = $element->getElementsByTagName("City");
			$city = $cities->item(0)->nodeValue;
	
			$counties = $element->getElementsByTagName("County");
			$county = $counties->item(0)->nodeValue;
		
			$skierelements = $xpath->query("/SkierLogs/Season[@fallYear ='2015']/Skiers[@clubId = '$id']/Skier");
			
			foreach ($skierelements as $skierelement){
				$year = 2015;
				$username = $skierelement->getAttribute("userName");
				$skier2015[] = new Register($username,$year);
			}
		
			$skierelements = $xpath->query("/SkierLogs/Season[@fallYear ='2016']/Skiers[@clubId = '$id']/Skier");
				foreach($skierelements as $skierelement){
				$year = 2016;
				$username = $skierelement->getAttribute("userName");
				$skier2016[] = new Register($username,$year);
				}
			$clubs[] = new Club($id,$name,$city,$county,$skier2015,$skier2016);
		}
		
		
		return $clubs;
	}

	/*
	READS USERNAMES OF SKIERS WHO HAS NO CLUB ASSIGNED TO THEM FOR THAT PARTICULAR SEASON.
	RETURNS AN ARRAY OF REGISTER OBJECTS.
	*/
	public function skiersnoclub() {
		$doc = new DOMDocument();
		$doc->load('model/SkierLogs.XML');
		$xpath = new DOMXPath($doc);
		
		$skierelements = $xpath->query("/SkierLogs/Season[@fallYear ='2016']/Skiers[not(@clubId)]/Skier");
		foreach($skierelements as $skierelement){
				$year = 2016;
				$username = $skierelement->getAttribute("userName");
				$skiers[] = new Register($username,$year);
				
		
		}
		$skierelements = $xpath->query("/SkierLogs/Season[@fallYear ='2015']/Skiers[not(@clubId)]/Skier");
		foreach($skierelements as $skierelement){
				$year = 2015;
				$username = $skierelement->getAttribute("userName");
				$skiers[] = new Register($username,$year);
				
		
		}
		return $skiers;
		
		
	}
}
?>