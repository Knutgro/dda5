<?php


class Skier {
	public $username;
	public $firstName;
	public $lastName;
	public $yearOfBirth;
	public $entries;
	public $total2015;
	public $total2016;
		
		
	public function __construct($username, $firstName, $lastName, $yearOfBirth,$entries,$total2015,$total2016)  
    {  
        $this->username = $username;
        $this->firstName = $firstName;
	    $this->lastName = $lastName;
	    $this->yearOfBirth = $yearOfBirth;
		$this->entries = $entries;
		$this->total2015 = $total2015;
		$this->total2016 = $total2016;
    } 

	
}


?>