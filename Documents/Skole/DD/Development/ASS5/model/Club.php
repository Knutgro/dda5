<?php


class Club {
	public $id;
	public $name;
	public $city;
	public $county;
	public $skiers2015;			//array of register objects
	public $skiers2016;			//array of register objects
		
		
	public function __construct($id, $name, $city, $county,$skiers2015,$skiers2016)  
    {  
        $this->id = $id;
        $this->name = $name;
	    $this->city = $city;
	    $this->county = $county;
		$this->skiers2015 = $skiers2015;		
		$this->skiers2016 = $skiers2016;           
    } 

	
}


?>