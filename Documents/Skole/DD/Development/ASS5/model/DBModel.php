<?php

include_once("Skier.php");
include_once("Entry.php");
include_once("Club.php");
include_once("Register.php");

class DBModel
{        
	
    protected $db = null;  
    
/* Connects to database unless its already connected */
    public function __construct($db = null)  
    { 
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try
			{                                                                         
				$this->db = new PDO('mysql:host=127.0.0.1;dbname=dd5','root','');
				
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch (PDOException $e)
			{
				echo 'connection failed:  '. $e->getMessage();                             // error appears if connection fails
			}
		}
    }
	
    /*Inserts skiers to seasonregister if they are not assigned to any clubs for the season. */
	public function insertSkiersNoClubs(array $skiersreg)
	{
		try
		{
			foreach($skiersreg as $skiers) {
				foreach($skiers as $skier) {
				
				$sql = "INSERT INTO seasonregister (seasonYear,clubId,userName) VALUES ('$skier->year',NULL,'$skier->username')";
				$stmt = $this->db->prepare($sql);
				$stmt->execute();
						
				}		
			}
		echo "<br>Successfully inserted skiers without clubs.<br>";
								
		}
		
		catch (PDOException $e)
		{
			echo 'Failed to insert skiers without clubs   '. $e->getMessage();
		}
	}
	
	
	/*Inserts skiers to skier table, years to season table and fills the seasondistance table with data*/
    public function insertSkiers(array $skiers)
    {
		
		try
		{
			
			$sql = "INSERT INTO season (seasonYear) VALUES ('2015'),('2016')";		
			$this->db->query($sql);
			
			foreach($skiers as $skier) {
				
				$sql = "INSERT INTO skier (userName,firstName,lastName,yearOfBirth) VALUES ('$skier->username','$skier->firstName','$skier->lastName','$skier->yearOfBirth')";
				$this->db->query($sql);
				
				$sql = "INSERT INTO seasondistance (seasonYear,userName,totalDist) VALUES ('2015','$skier->username','$skier->total2015'),('2016','$skier->username','$skier->total2016')";
				$this->db->query($sql);
				
				foreach($skier->entries as $entry) {
					$sql = "INSERT INTO entry (eDate,area,distance,userName) VALUES ('$entry->date','$entry->area','$entry->distance','$skier->username')";
					$this->db->query($sql);
	
				}	
			}	
			echo "<br>Successfully inserted skiers.<br>";			
		}
		catch (PDOException $e)
		{
			echo 'Failed to insert skiers   '. $e->getMessage();
		}
    }
    
	/*Populates club table with clubs, fills the seasonregister table with skiers, their assigned clubs and the year relevant to the register.*/
    public function insertClubs($clubs)
    {
		try
		{
			
			
			foreach($clubs as $club) {
				
				$sql = "INSERT INTO club (id,name,city,county) VALUES ('$club->id','$club->name','$club->city','$club->county')";
				$stmt = $this->db->prepare($sql);
				$stmt->execute();
				//echo "<br>$club->id - $club->name - $club->city - $club->county <br> ";
				
				foreach($club->skiers2015 as $skiers2015) {
					$sql = "INSERT INTO seasonregister (seasonYear,clubId,userName) VALUES ('$skiers2015->year','$club->id','$skiers2015->username')";
					$stmt = $this->db->prepare($sql);
					$stmt->execute();
				}
				
				foreach($club->skiers2016 as $skiers2016) {
					$sql = "INSERT INTO seasonregister (seasonYear,clubId,userName) VALUES ('$skiers2016->year','$club->id','$skiers2016->username')";
					$stmt = $this->db->prepare($sql);
					$stmt->execute();
				}				
			}
			echo "<br>Successfully inserted clubs and skiers assigned to it pr season.<br>";		
				
		}
		catch (PDOException $e)
		{
			echo 'Failed to insert clubs   '. $e->getMessage();
		}
    }
}

?>