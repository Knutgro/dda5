<?php
include_once("model/XMLModel.php");
include_once("model/DBModel.php");


class Controller {
	public $xmlmodel;
	public $dbmodel;
	
	public static $OP_PARAM_NAME = 'op';
	public static $DEL_OP_NAME = 'del';
	public static $ADD_OP_NAME = 'add';
	public static $MOD_OP_NAME = 'mod';
	
	public function __construct()  
    {  
		session_start();
        $this->xmlmodel = new DOMmodel();
		$this->dbmodel = new DBModel();
    } 
	
	public function invoke() {
																//Reads from xml file and stores it in memory
		$skiers = $this->xmlmodel->readSkiers();
		$clubs = $this->xmlmodel->readClubs();
		$regSkiers[] = $this->xmlmodel->skiersnoclub();
																//Inserts data into database
		$this->dbmodel->insertSkiers($skiers);
		$this->dbmodel->insertClubs($clubs);
		$this->dbmodel->insertSkiersNoClubs($regSkiers);
		echo "<br>All data from the XML file is uploaded to the database!<br>";
	}
}
?>